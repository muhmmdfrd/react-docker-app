FROM node:16.17.0-bullseye-slim AS development

WORKDIR /app
 
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json

RUN npm ci

COPY . /app

FROM development AS build

RUN npm run build

FROM nginx:alpine

RUN nginx -t

COPY --from=build /app/.nginx/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html

RUN rm -rf ./*

COPY --from=build /app/dist .

ENTRYPOINT ["nginx", "-g", "daemon off;"]

