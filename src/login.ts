function login(usr: string, pwd: string): Promise<Response> {
  const data: { username: string; password: string } = {
    username: usr,
    password: pwd,
  };

  const options: RequestInit = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  };

  return fetch("http://localhost:82/api/login", options);
}

export default login;
