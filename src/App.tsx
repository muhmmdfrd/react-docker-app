import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import login from "./login";

function App() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const auth = () => {
    login(username, password)
      .then((response) => response.json())
      .then((data: { message: string; status: number; success: boolean }) => {
        if (data.success) {
          alert(data.message);
        } else {
          alert(data.message);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React + Docker + Jenkins</h1>
      <form>
        <input
          type="text"
          placeholder="username"
          onChange={(v) => setUsername(v.target.value)}
          value={username}
        />
        <br />
        <input
          type="password"
          placeholder="password"
          onChange={(v) => setPassword(v.target.value)}
          value={password}
        />
        <br />
      </form>
      <button role="button" onClick={() => auth()}>
        Login
      </button>
    </>
  );
}

export default App;
